var StatusLayer = cc.Layer.extend({
	labelCoin:null,
	labelMeter:null,
	meter:0,
	coins:0,

	ctor:function () {
		this._super();
		this.init();
	},

	init:function () {
		this._super();

		var winsize = cc.director.getWinSize();

		this.labelCoin = new cc.LabelTTF("Coins:0", "Helvetica", 20);
		this.labelCoin.setColor(cc.color(0,0,0));//black color
		this.labelCoin.setPosition(cc.p(70, winsize.height - 20));
		this.addChild(this.labelCoin);

		this.labelMeter = new cc.LabelTTF("0M", "Helvetica", 20);
		this.labelMeter.setPosition(cc.p(winsize.width - 70, winsize.height - 20));
		this.addChild(this.labelMeter);
		
		
		// goto main menu
		var winSize = cc.director.getWinSize();

		var centerPos = cc.p(winSize.width / 2, winSize.height - 50);
		cc.MenuItemFont.setFontSize(30);
		var menuItemRestart = new cc.MenuItemSprite(
				new cc.Sprite(res.pause_button),
				new cc.Sprite(res.pause_button),
				function(){
					cc.director.pause();
					this.addChild(new GameMenuLayer);
				},this);
		menuItemRestart.setScale(0.3)
		
		var menu = new cc.Menu(menuItemRestart);
		menu.setPosition(centerPos);
		this.addChild(menu);
	},
	updateMeter:function () {
		this.meter += 0.1;
		this.labelMeter.setString(this.meter.toFixed(0) + "M");
	}
});