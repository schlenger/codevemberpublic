var BackgroundLayer = cc.Layer.extend({
	ctor:function () {
		this._super();
		this.init();
		
		this.scheduleUpdate();
	},

	init:function () {
		this._super();
		var winsize = cc.director.getWinSize();

		//create the background image and position it at the center of screen
		var centerPos = cc.p(winsize.width / 2, winsize.height / 2);
		var sky = new cc.Sprite(res.bg_sky);
		sky.setPosition(centerPos);
		this.addChild(sky);
		
		
		var clouds = new cc.Sprite(res.bg_clouds);
		clouds.setPosition(cc.p(winsize.width*1.5, winsize.height /2 +100));
		this.addChild(clouds);
		
		var clouds2 = new cc.Sprite(res.bg_clouds2);
		clouds2.setPosition(cc.p(winsize.width*1.5, winsize.height /2 +80));
		this.addChild(clouds2);
		
		var cloudsMove = new cc.MoveBy(8, cc.p(-winsize.width*2, 0));
		var cloudsMoveFast = new cc.MoveBy(6, cc.p(-winsize.width*2, 0));
		var cloudsJump = new cc.MoveBy(0, cc.p(winsize.width*2, 0));
		//spriteBG3.runAction();
		
		var seq = cc.repeatForever(cc.sequence([cc.delayTime(4),cloudsMove,cloudsJump]));
		var seq2 = cc.repeatForever(cc.sequence([cloudsMove.clone(),cc.delayTime(2),cloudsJump.clone(), cloudsMoveFast.clone(), cloudsJump.clone()]));
		clouds.runAction(seq);
		clouds2.runAction(seq2);
		
	},
	update : function(dt) {
		
	}
});