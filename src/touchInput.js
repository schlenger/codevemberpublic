/*
 *  --touchListener implementation--
 * usage: cc.eventManager.addListener(touchListener, {mainScreen})
 *  ==> if screen touched, perform related methods
 */
setupListener = function(player, scene) {
	var touchListener = {
			//only recognize touch by touch, no multi-touch
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			//character took-off?
			charAirborne : false,
			//stick is being modified?
			stickModified :  false, // necessary?
			//character has passed highest point?
			topPointPassed : false,

			//touch downs
			onTouchBegan: function(touch, event) {
				//character is still on the ground -> sizing stick length
				if (!this.charAirborne) {
					//sprite.setPosition(100, 100)
					//(1) give minimum stick length
					//(2) grow stick length
					//(3) end with maximum stick length
				}
				//character is up in the air -> possible second jump
				else {
					//character passed highest point -> char performs second jump
					if (this.topPointPassed) {
						//push away from stick
					}
					//character hasn't passed highest point -> char falls down
					else {

					}

				}
				return true;
			},

			//touch down
			onTouchEnded: function(touch, event) {
				//only call action for growing stick mode
				if (!this.charAirborne && this.stickModified) {
					//hit stick to the ground
				}
			}
	}


	var mouseListener = {
			//only recognize touch by touch, no multi-touch
			event: cc.EventListener.MOUSE,
			//character took-off?
			charAirborne : false,
			//stick is being modified?
			stickModified :  false, // necessary?
			//character has passed highest point?
			topPointPassed : false,

			//touch downs
			onMouseDown: function(event) {

				var target = event.getCurrentTarget()
				//console.log(target);
				//console.log(target.sprite);
				if(scene.jump == 0) {
					player.applyImpulse(cp.v(0, 250), cp.v(0, 0));//run speed
					scene.jump += 60;
				}
				
				
				//target.x += 100
				//target.y += 100

				//character is still on the ground -> sizing stick length
				if (!this.charAirborne) {
					//(1) give minimum stick length
					//(2) grow stick length
					//(3) end with maximum stick length
				}
				//character is up in the air -> possible second jump
				else {
					//character passed highest point -> char performs second jump
					if (this.topPointPassed) {
						//push away from stick
					}
					//character hasn't passed highest point -> char falls down
					else {

					}

				}
			},

			//touch down
			onMouseUp: function(event) {
				//only call action for growing stick mode
				if (!this.charAirborne && this.stickModified) {
					//hit stick to the ground
				}
			}
	}
	
	// return the json structered listener data
	return { mouse : mouseListener,
		touch :touchListener};
}

